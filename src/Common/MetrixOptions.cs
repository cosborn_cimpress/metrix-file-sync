namespace Common;

/// <summary>The application's options for Metrix configuration.</summary>
public abstract class MetrixOptions
    : IOptionsSnapshot<MetrixOptions>
{
    /// <summary>The default name of the configuration section.</summary>
    public const string Metrix = nameof(Metrix);

    /// <summary>Gets the name of the bucket to which to upload exports.</summary>
    public TimeSpan SyncDelay { get; init; } = TimeSpan.FromSeconds(5);

    /// <inheritdoc/>
    MetrixOptions IOptions<MetrixOptions>.Value => this;

    /// <inheritdoc/>
    MetrixOptions IOptionsSnapshot<MetrixOptions>.Get(string? name) => this;
}
