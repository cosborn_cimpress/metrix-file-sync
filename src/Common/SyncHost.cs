namespace Common;

/// <summary>Provides convenience methods for creating instances of <see cref="IHostBuilder"/> with pre-configured defaults.</summary>
public static class SyncHost
{
    /// <summary>Initializes a new instance of the <see cref="HostApplicationBuilder"/> class with pre-configured defaults.</summary>
    /// <typeparam name="TOptions">The type of the workers' application options.</typeparam>
    /// <param name="args">The workers' runner's command-line arguments.</param>
    /// <param name="serviceName">The name of the service to report for logging.</param>
    /// <returns>The configured host application builder.</returns>
    public static HostApplicationBuilder CreateApplicationBuilder<TOptions>(string[]? args, string serviceName)
        where TOptions : MetrixOptions
    {
        var builder = Host.CreateApplicationBuilder(args);
        _ = builder.Services
            .AddOptions<TOptions>()
            .BindConfiguration(MetrixOptions.Metrix)
            .ValidateDataAnnotations()
            .Validate(o => o.SyncDelay > TimeSpan.Zero, "Sync delay must be strictly positive.")
            .ValidateOnStart();
        _ = builder.Services
            .AddOptions<JsonSerializerOptions>()
            .BindConfiguration(nameof(JsonSerializer))
            .Configure(o =>
            {
                o.DictionaryKeyPolicy = CamelCase;
                o.PropertyNamingPolicy = CamelCase;
            });
        _ = builder.Services
            .AddWindowsService(o => o.ServiceName = serviceName)
            .AddAWSService<IAmazonS3>()
            .AddSingleton<ITransferUtility>(p => new TransferUtility(p.GetRequiredService<IAmazonS3>()));
        return builder;
    }
}
