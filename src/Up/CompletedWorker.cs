using static System.Net.Mime.DispositionTypeNames;
using static Up.IExport;

namespace Up;

/// <summary>The background worker which will sync completed files up to Amazon S3.</summary>
public sealed partial class CompletedWorker
    : BackgroundService
{
    readonly ITransferUtility _transferUtility;
    readonly IOptionsMonitor<MetrixOptions> _metrixOpts;
    readonly ILogger _logger;

    /// <summary>Initializes a new instance of the <see cref="CompletedWorker"/> class.</summary>
    /// <param name="transferUtility">A high-level client for interacting with Amazon S3.</param>
    /// <param name="metrixOpts">The application's options for Metrix configuration.</param>
    /// <param name="logger">An application logger specialized for <see cref="CompletedWorker"/>.</param>
    public CompletedWorker(ITransferUtility transferUtility, IOptionsMonitor<MetrixOptions> metrixOpts, ILogger<CompletedWorker> logger)
    {
        _transferUtility = transferUtility;
        _metrixOpts = metrixOpts;
        _logger = logger;
    }

    /// <inheritdoc/>
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        try
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await ExecuteAsyncCore(_metrixOpts.CurrentValue, stoppingToken).ConfigureAwait(false);
            }
        }
        catch (Exception e) when (e is not TaskCanceledException)
        {
            Failure(e);
        }
    }

    async Task ExecuteAsyncCore(MetrixOptions metrixOpts, CancellationToken stoppingToken)
    {
        var completedDirectory = Path.Combine(metrixOpts.RootDirectory, "Completed");
        var snapshot = Directory.GetFiles(completedDirectory, "*.mxml", SearchOption.TopDirectoryOnly);

        /* note(cosborn)
         * This delay serves two purposes:
         * 1. We don't want the loop to execute *as fast as possible* – that would consume a CPU core.
         * 2. Windows only guarantees atomicity in file moves within the same drive. These files
         *    are *probably* moving from C: to D:, so give it a sec. Just in case.
         */
        await Task.Delay(metrixOpts.SyncDelay, stoppingToken).ConfigureAwait(false);
        foreach (var (input, uploads) in snapshot.Select(f => new FileInfo(f)).SelectZip(CreateExport))
        {
            foreach (var (file, mimeType) in uploads)
            {
                try
                {
                    await _transferUtility.UploadAsync(
                        new()
                        {
                            BucketName = metrixOpts.OutputBucketName,
                            FilePath = file.FullName,
                            Headers =
                            {
                                ContentDisposition = new ContentDispositionHeaderValue(Inline)
                                {
                                    FileName = file.Name,
                                }.ToString(),
                                ContentType = mimeType,
                            },
                            Key = metrixOpts.ToKey(file.FullName),
                        },
                        stoppingToken).ConfigureAwait(false);
                    Synced(file.FullName, input.FullName);
                }
                catch (AmazonS3Exception as3e)
                {
                    FailedToSync(as3e, file.FullName, input.FullName);
                    continue;
                }

                file.Delete();
            }

            input.Delete();
        }
    }

    IEnumerable<Export> CreateExport(FileInfo inputFileInfo)
    {
        var root = XElement.Load(inputFileInfo.FullName);
        if (root.Element(Project)?.Attribute(ProjectId) is not { } projectId)
        {
            NoProjectId(inputFileInfo.FullName);
            yield break;
        }

        if (root.Element(CommandPool)?.Element(Run) is not { } run)
        {
            NoRun(inputFileInfo.FullName);
            yield break;
        }

        var mxmlExports = CreateExports<Mxml>((string)projectId, run);
        var zipExports = CreateExports<Zip>((string)projectId, run);
        foreach (var export in mxmlExports.Concat(zipExports))
        {
            yield return export;
        }

        static IEnumerable<Export> CreateExports<T>(string projectId, XElement run)
            where T : IExport<T> => run.Elements(T.Element)
            .Select(static e => e.Attribute(DestinationFolder))
            .OfType<XAttribute>()
            .Select(df => Path.Combine((string)df, projectId + T.Extension))
            .Select(static p => new FileInfo(p))
            .Select(e => new Export(e, T.MimeType));
    }

    [LoggerMessage(1, Error, "Failed to locate project ID in '{Output:l}'!")]
    partial void NoProjectId(string output);

    [LoggerMessage(2, Error, "Failed to locate 'Run' section in '{Output:l}'!")]
    partial void NoRun(string output);

    [LoggerMessage(3, Error, "Failed to sync '{Export:l}' as exported from '{Output:l}'!")]
    partial void FailedToSync(Exception e, string export, string output);

    [LoggerMessage(4, Information, "Synced '{Export:l}' as exported from '{Output:l}'.")]
    partial void Synced(string export, string output);

    [LoggerMessage(999, Critical, "Exiting due to error!")]
    partial void Failure(Exception e);

    sealed record class Export(FileInfo File, string MimeType);
}
