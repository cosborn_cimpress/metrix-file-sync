namespace System.Linq;

/// <summary>Extensions to the functionality of the <see cref="IEnumerable{T}"/> interface.</summary>
public static class UpEnumerableExtensions
{
    /// <summary>Projects each element of a sequence into a new form alongside the original input.</summary>
    /// <typeparam name="TSource">The type of the elements of <paramref name="source"/>.</typeparam>
    /// <typeparam name="TResult">The type of the value returned by <paramref name="selector"/>.</typeparam>
    /// <param name="source">A sequence of values to invoke a transform function on.</param>
    /// <param name="selector">A transform function to apply to each element.</param>
    /// <returns>
    /// An <see cref="IEnumerable{T}"/> whose elements are tuples whose members are each element of
    /// <paramref name="source"/> and the result of invoking the transform function on each element of
    /// <paramref name="source"/>.
    /// </returns>
    public static IEnumerable<(TSource Source, TResult Result)> SelectZip<TSource, TResult>(
        this IEnumerable<TSource> source,
        Func<TSource, TResult> selector) => source.Select(s => (Source: s, Result: selector(s)));
}
