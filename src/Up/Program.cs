var builder = SyncHost.CreateApplicationBuilder<Up.MetrixOptions>(args, "Metrix File Sync (Up)");
_ = builder.Services
    .AddHostedService<CompletedWorker>()
    .AddHostedService<FailedWorker>();

using var host = builder.Build();
await host.RunAsync().ConfigureAwait(false);
