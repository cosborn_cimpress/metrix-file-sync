namespace Up;

/// <summary>The information about a kind of file which can be exported from Metrix.</summary>
/// <typeparam name="T">The kind of file which is being exported.</typeparam>
interface IExport<T>
    : IExport
    where T : IExport<T>
{
    /// <summary>Gets the name of the element which defines this kind of export.</summary>
    static abstract XName Element { get; }

    /// <summary>Gets the file extension associated with this kind of export.</summary>
    /// <remark>This value includes the dot.</remark>
    static abstract string Extension { get; }

    /// <summary>Gets the MIME type associated with this kind of export.</summary>
    static abstract string MimeType { get; }
}
