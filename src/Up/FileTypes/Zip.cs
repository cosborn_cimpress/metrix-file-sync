namespace Up;

/// <summary>An export of a zip archive.</summary>
sealed class Zip
    : IExport<Zip>
{
    /// <inheritdoc/>
    public static XName Element { get; } = XName.Get("ExportZipFile", IExport.Xmlns);

    /// <inheritdoc/>
    public static string Extension => ".zip";

    /// <inheritdoc/>
    public static string MimeType => Application.Zip;
}
