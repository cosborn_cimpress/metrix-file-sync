namespace Up;

/// <summary>The information about a kind of file which can be exported from Metrix.</summary>
interface IExport
{
    /// <summary>The namespace of an export directive.</summary>
    public const string Xmlns = "http://www.lithotechnics.com";

    /// <summary>Gets the name of an MXML Project element.</summary>
    public static XName Project { get; } = XName.Get("Project", Xmlns);

    /// <summary>Gets the name of an MXML ProjectID attribute.</summary>
    public static XName ProjectId => "ProjectID";

    /// <summary>Gets the name of an MXML CommandPool element.</summary>
    public static XName CommandPool { get; } = XName.Get("CommandPool", Xmlns);

    /// <summary>Gets the name of an MXML Run element.</summary>
    public static XName Run { get; } = XName.Get("Run", Xmlns);

    /// <summary>Gets the name of an MXML DestinationFolder attribute.</summary>
    public static XName DestinationFolder => "DestinationFolder";
}
