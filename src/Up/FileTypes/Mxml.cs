namespace Up;

/// <summary>An export of MXML.</summary>
sealed class Mxml
    : IExport<Mxml>
{
    /// <inheritdoc/>
    public static XName Element { get; } = XName.Get("ExportMetrixXML", IExport.Xmlns);

    /// <inheritdoc/>
    public static string Extension => ".mxml";

    /// <inheritdoc/>
    public static string MimeType => Application.Xml;
}
