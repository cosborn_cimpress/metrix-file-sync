namespace Up;

/// <summary>The application's options for Metrix configuration.</summary>
public sealed class MetrixOptions
    : Common.MetrixOptions, IOptionsSnapshot<MetrixOptions>
{
    /// <summary>Gets the path to the directory in which to look for exports.</summary>
    public string ExportDirectory => Path.Combine(RootDirectory, "Export");

    /// <summary>Gets the path to the directory to watch for completion events.</summary>
    [Required]
    public string RootDirectory { get; init; } = string.Empty;

    /// <summary>Gets the name of the bucket to which to upload exports.</summary>
    [Required]
    public string OutputBucketName { get; init; } = string.Empty;

    /// <inheritdoc/>
    MetrixOptions IOptions<MetrixOptions>.Value => this;

    /// <summary>Converts the full path to a file into a key suitable for AWS S3.</summary>
    /// <param name="fullName">The full path to a file.</param>
    /// <returns>A key corresponding to the provided path.</returns>
    public string ToKey(string fullName) => Path
        .GetRelativePath(ExportDirectory, fullName)
        .Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

    /// <inheritdoc/>
    MetrixOptions IOptionsSnapshot<MetrixOptions>.Get(string? name) => this;
}
