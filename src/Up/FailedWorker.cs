using static System.Net.Mime.DispositionTypeNames;

namespace Up;

/// <summary>The background worker which will sync failed files up to Amazon S3.</summary>
public sealed partial class FailedWorker
    : BackgroundService
{
    readonly ITransferUtility _transferUtility;
    readonly IOptionsMonitor<MetrixOptions> _metrixOpts;
    readonly ILogger _logger;

    /// <summary>Initializes a new instance of the <see cref="FailedWorker"/> class.</summary>
    /// <param name="transferUtility">A high-level client for interacting with Amazon S3.</param>
    /// <param name="metrixOpts">The application's options for Metrix configuration.</param>
    /// <param name="logger">An application logger specialized for <see cref="CompletedWorker"/>.</param>
    public FailedWorker(ITransferUtility transferUtility, IOptionsMonitor<MetrixOptions> metrixOpts, ILogger<FailedWorker> logger)
    {
        _transferUtility = transferUtility;
        _metrixOpts = metrixOpts;
        _logger = logger;
    }

    /// <inheritdoc/>
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        try
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await ExecuteAsyncCore(_metrixOpts.CurrentValue, stoppingToken).ConfigureAwait(false);
            }
        }
        catch (Exception e) when (e is not TaskCanceledException)
        {
            Failure(e);
        }
    }

    async Task ExecuteAsyncCore(MetrixOptions metrixOpts, CancellationToken stoppingToken)
    {
        var failedDirectory = Path.Combine(metrixOpts.ExportDirectory, "Failed");
        var snapshot = Directory.GetFiles(failedDirectory, "*.mxml", SearchOption.TopDirectoryOnly);

        /* note(cosborn)
         * This delay serves two purposes:
         * 1. We don't want the loop to execute *as fast as possible* – that would consume a CPU core.
         * 2. Windows only guarantees atomicity in file moves within the same drive. These files
         *    are *probably* moving from C: to D:, so give it a sec. Just in case.
         */
        await Task.Delay(metrixOpts.SyncDelay, stoppingToken).ConfigureAwait(false);
        foreach (var (input, mimeType) in snapshot.Select(CreateExport))
        {
            try
            {
                await _transferUtility.UploadAsync(
                    new()
                    {
                        BucketName = metrixOpts.OutputBucketName,
                        FilePath = input.FullName,
                        Headers =
                        {
                            ContentDisposition = new ContentDispositionHeaderValue(Inline)
                            {
                                FileName = input.Name,
                            }.ToString(),
                            ContentType = mimeType,
                        },
                        Key = metrixOpts.ToKey(input.FullName),
                    },
                    stoppingToken).ConfigureAwait(false);
                Synced(input.FullName);
            }
            catch (AmazonS3Exception as3e)
            {
                FailedToSync(as3e, input.FullName);
                continue;
            }

            input.Delete();
        }
    }

    Export CreateExport(string inputFilePath) => new(new(inputFilePath), Mxml.MimeType);

    [LoggerMessage(1, Error, "Failed to sync '{Input:l}'!")]
    partial void FailedToSync(Exception e, string input);

    [LoggerMessage(2, Information, "Synced '{input:l}'.")]
    partial void Synced(string input);

    [LoggerMessage(999, Critical, "Exiting due to error!")]
    partial void Failure(Exception e);

    sealed record class Export(FileInfo InputFile, string MimeType);
}
