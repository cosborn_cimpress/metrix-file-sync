var hostBuilder = SyncHost.CreateApplicationBuilder<Down.MetrixOptions>(args, "Metrix File Sync (Down)");
_ = hostBuilder.Services
    .AddHostedService<Worker>()
    .AddAWSService<IAmazonStepFunctions>();

using var host = hostBuilder.Build();
await host.RunAsync().ConfigureAwait(false);
