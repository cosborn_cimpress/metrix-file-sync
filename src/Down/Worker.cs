namespace Down;

/// <summary>The background worker which will sync files up to Amazon S3.</summary>
public sealed partial class Worker
    : BackgroundService
{
    readonly ITransferUtility _transferUtility;
    readonly IAmazonStepFunctions _stepFunctions;
    readonly IOptionsMonitor<MetrixOptions> _metrixOpts;
    readonly JsonSerializerOptions _jsonOpts;
    readonly ILogger _logger;

    /// <summary>Initializes a new instance of the <see cref="Worker"/> class.</summary>
    /// <param name="transferUtility">A high-level client for interacting with Amazon S3.</param>
    /// <param name="stepFunctions">A client for interacting with AWS Step Functions.</param>
    /// <param name="metrixOpts">The application's options for Metrix configuration.</param>
    /// <param name="jsonOpts">The application's options for JSON serialization and deserialization.</param>
    /// <param name="logger">An application logger specialized for <see cref="Worker"/>.</param>
    public Worker(
        ITransferUtility transferUtility,
        IAmazonStepFunctions stepFunctions,
        IOptionsMonitor<MetrixOptions> metrixOpts,
        IOptions<JsonSerializerOptions> jsonOpts,
        ILogger<Worker> logger)
    {
        ArgumentNullException.ThrowIfNull(jsonOpts);

        _transferUtility = transferUtility;
        _stepFunctions = stepFunctions;
        _metrixOpts = metrixOpts;
        _jsonOpts = jsonOpts.Value;
        _logger = logger;
    }

    /// <inheritdoc/>
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        try
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await ExecuteAsyncCore(_metrixOpts.CurrentValue, stoppingToken).ConfigureAwait(false);
            }
        }
        catch (Exception e) when (e is not TaskCanceledException)
        {
            Failure(e);
        }
    }

    async Task ExecuteAsyncCore(MetrixOptions metrixOpts, CancellationToken stoppingToken)
    {
        /* note(cosborn)
         * Here we lack a delay as a strategic decision based on the behavior of GetActivityTaskAsync.
         * This is a long-polling API, so when we initiate the connection, it will stay open for 60
         * seconds if no work is available. Waiting `taskDelay` after already waiting 60 seconds
         * seems pointless, and waiting `taskDelay` after retrieving work seems inefficient. (The
         * most likely time to find work is immediately after previous work.)
         *
         * So GetActivityTaskAsync here acts as our busy–delay.
         */

        var activityTask = await _stepFunctions.GetActivityTaskAsync(
            new()
            {
                ActivityArn = metrixOpts.ActivityArn,
                WorkerName = "Metrix File Sync",
            },
            stoppingToken).ConfigureAwait(false);
        if (activityTask is not { Input: { } input, TaskToken: { } taskToken })
        {
            /*
             * note(cosborn) A `null` TaskToken value after a poll for an activity task
             * indicates that there is no work available. Go 'round again.
             */
            return;
        }

        if (JsonSerializer.Deserialize<Input>(input, _jsonOpts) is not var (bucketName, key))
        {
            InvalidInput(taskToken);
            await SendTaskFailureAsync(
                cause: "Input did not match expected structure!",
                error: "Metrix.InvalidInput",
                taskToken,
                stoppingToken).ConfigureAwait(false);
            return;
        }

        try
        {
            await _transferUtility.DownloadAsync(
                new()
                {
                    BucketName = bucketName,
                    FilePath = Path.Combine(metrixOpts.InputDirectory, key.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar)),
                    Key = key,
                },
                stoppingToken).ConfigureAwait(false);
        }
        catch (AmazonS3Exception as3e)
        {
            FailedToSync(as3e, bucketName, key);
            await SendTaskFailureAsync(
                cause: as3e.Message,
                error: "Metrix.SyncFailure",
                taskToken,
                stoppingToken).ConfigureAwait(false);
            return;
        }

        try
        {
            _ = await _stepFunctions.SendTaskSuccessAsync(
                new()
                {
                    TaskToken = taskToken,
                },
                stoppingToken).ConfigureAwait(false);
        }
        catch (Exception e)
        {
            FailedToSendSuccess(e, taskToken);
            return;
        }

        Synced(key);
    }

    async Task SendTaskFailureAsync(string cause, string error, string taskToken, CancellationToken cancellationToken)
    {
        try
        {
            _ = await _stepFunctions.SendTaskFailureAsync(
                new()
                {
                    Cause = cause,
                    Error = error,
                    TaskToken = taskToken,
                },
                cancellationToken).ConfigureAwait(false);
        }
        catch (Exception e)
        {
            FailedToSendFailure(e, taskToken);
        }
    }

    [LoggerMessage(1, Error, "Invalid input for activity task with token '{TaskToken:l}'!")]
    partial void InvalidInput(string taskToken);

    [LoggerMessage(2, Error, "Failed to sync file from '{BucketName:l}/{Key:l}'!")]
    partial void FailedToSync(Exception e, string bucketName, string key);

    [LoggerMessage(3, Error, "Failed to send failure to activity task with token '{TaskToken:l}'!")]
    partial void FailedToSendFailure(Exception e, string taskToken);

    [LoggerMessage(4, Information, "Synced '{Key:l}'.")]
    partial void Synced(string key);

    [LoggerMessage(5, Error, "Failed to send success to activity task with token '{TaskToken:l}'!")]
    partial void FailedToSendSuccess(Exception e, string taskToken);

    [LoggerMessage(999, Critical, "Exiting due to error!")]
    partial void Failure(Exception e);

    sealed record class Input(string BucketName, string Key);
}
