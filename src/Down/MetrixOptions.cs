namespace Down;

/// <summary>The application's options for Metrix configuration.</summary>
public sealed class MetrixOptions
    : Common.MetrixOptions, IOptionsSnapshot<MetrixOptions>
{
    /// <summary>Gets the ARN of the activity to poll for input requests.</summary>
    [Required]
    public string ActivityArn { get; init; } = string.Empty;

    /// <summary>Gets the path to the directory to which to write inputs.</summary>
    [Required]
    public string InputDirectory { get; init; } = string.Empty;

    /// <summary>Gets the name of the bucket from which to download imports.</summary>
    [Required]
    public string InputBucketName { get; init; } = string.Empty;

    /// <inheritdoc/>
    MetrixOptions IOptions<MetrixOptions>.Value => this;

    /// <inheritdoc/>
    MetrixOptions IOptionsSnapshot<MetrixOptions>.Get(string? name) => this;
}
